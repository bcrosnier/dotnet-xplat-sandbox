Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"
Push-Location (Split-Path $MyInvocation.MyCommand.Path) # cd to script directory
[Environment]::CurrentDirectory = Get-Location -PSProvider FileSystem

dotnet restore
if ($LASTEXITCODE -ne 0) { Throw "dotnet restore exited with code $LASTEXITCODE" }

dir test | % {
  dotnet test $_.FullName
  if ($LASTEXITCODE -ne 0) { Throw "dotnet test $($_.FullName) exited with code $LASTEXITCODE" }
}

Pop-Location
[Environment]::CurrentDirectory = Get-Location -PSProvider FileSystem   