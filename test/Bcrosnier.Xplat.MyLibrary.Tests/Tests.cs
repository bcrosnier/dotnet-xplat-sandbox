﻿using System;
using Xunit;
using FluentAssertions;

namespace Bcrosnier.Xplat.MyLibrary.Tests
{
    public class MyLibraryTests
    {
        [Fact]
        public void Test1() 
        {
            true.Should().BeTrue();
            true.ToString().Should().NotBe("false");
        }
        
        [Fact]
        public void Test2() 
        {
            MyClass.MyMethod().Should().BeTrue();
        }
    }
}
