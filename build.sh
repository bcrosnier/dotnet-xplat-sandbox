#!/bin/bash
pushd "${0%/*}" # cd to script directory

dotnet restore
for D in test/*; do dotnet test "$D"; done

popd
